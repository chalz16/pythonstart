# -*- coding: utf-8 -*-
"""
Created on Thu Jan 31 14:17:32 2019

@author: charl
"""

"""Removed unnecessary assignments"""
import json
import requests
import pandas as pd
import datetime
r = requests.get("http://data.rcc-acis.org/StnData?sid=kbtr&sdate=20180101&edate=20181231&elems=maxt,mint,pcpn&output=json")
data =json.loads(r.text)
df =pd.DataFrame(data['data'])
df.columns = ['date','max','min','rain']
month = 0
i,days,first,last,temp = 0,0,0,0,1
fo = open("output.txt","w")
#replace T with 0 wherever present, for sake of finding total precipitation for each month
rep_rain = df['rain'].replace({'T': 0})
df_rain = pd.to_numeric(rep_rain)
#df2 = df.groupby(['Animal']).mean()
#print(df.groupby(['max']).mean())
print(df_rain)
mon = datetime.datetime.strptime(df['date'][i], "%Y-%m-%d").strftime("%m")
month = int(mon)
while i < (len(df) + 1):
    if i == len(df):
       pass
    else:
       mon = datetime.datetime.strptime(df['date'][i], "%Y-%m-%d").strftime("%m")
       temp = int(mon)
    if temp != month or i == len(df):
       last = i
       max_mean = pd.to_numeric(df['max'][first:(last)]).mean() #calculate avg max temp of that month
       min_mean = pd.to_numeric(df['min'][first:(last)]).mean() #calculate avg min temp of that month
       rain_sum = pd.to_numeric(rep_rain[first:(last)]).sum() #calculate total precipitation of that month
       display = datetime.datetime.strptime(df['date'][1], "%Y-%m-%d").strftime("%Y") + '-' + str(month) + ', ' + str(max_mean) + ', ' + str(min_mean) + ', ' + str(rain_sum) + '\n'
       #output the 3 paramters for each month into the text file
       fo.write(display)
       month = month + 1
       first = last               
    i = i + 1
fo.close()
     